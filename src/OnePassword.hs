{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude, TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE GADTs #-}

module OnePassword where

import Control.Lens ((^?))
import qualified Control.Lens as L
import Data.Aeson (Value, object, (.=))
import Data.Aeson.Lens
  ( AsPrimitive (_String)
  , key
  , _Integral
  )
import qualified Data.Text as T
import Data.Yaml (decodeFileThrow)
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Protolude hiding (FilePath, to)
import Servant.API (Capture, Get, Header', JSON, Post, ReqBody, Required, ToHttpApiData (toUrlPiece), type (:<|>) ((:<|>)), type (:>), Patch)
import Servant.Client
  ( BaseUrl (BaseUrl)
  , ClientError
  , ClientM
  , Scheme (Http)
  , client
  , mkClientEnv
  , runClientM
  )
import Turtle (FilePath, encodeString)

newtype VaultID = VaultID {vaultIDText :: Text} deriving newtype (ToHttpApiData, IsString, Show, Eq)

newtype ItemID = ItemID Text deriving newtype (ToHttpApiData, IsString, Show, Eq)

newtype Token = Token Text deriving newtype (IsString, Show, Eq)

instance ToHttpApiData Token where
  toUrlPiece (Token token) = "Bearer " <> T.strip token

data Item = Item {}

type Auth = Header' '[Required] "Authorization" Token

type OnePasswordAPI =
  Auth
    :> "v1"
    :> "vaults"
    :> Capture "vault" VaultID
    :> "items"
    :> ReqBody '[JSON] Value
    :> Post '[JSON] Value
    :<|> Auth
      :> "v1"
      :> "vaults"
      :> Capture "vault" VaultID
      :> "items"
      :> Get '[JSON] Value
    :<|> Auth
      :> "v1"
      :> "vaults"
      :> Capture "vault" VaultID
      :> "items"
      :> Capture "item" ItemID
      :> Get '[JSON] Value
    :<|> Auth
      :> "v1"
      :> "vaults"
      :> Get '[JSON] Value
    :<|> Auth
      :> "health"
      :> Get '[JSON] Value
    :<|> Auth
      :> "v1"
      :> "vaults"
      :> Capture "vault" VaultID
      :> "items"
      :> Capture "item" ItemID
      :> ReqBody '[JSON] Value
      :> Patch '[JSON] Value

postItem :: Token -> VaultID -> Value -> ClientM Value
getItems :: Token -> VaultID -> ClientM Value
getVaults :: Token -> ClientM Value
getItem :: Token -> VaultID -> ItemID -> ClientM Value
getHealth :: Token -> ClientM Value
patchItem :: Token -> VaultID -> ItemID -> Value -> ClientM Value
(postItem :<|> getItems :<|> getItem :<|> getVaults :<|> getHealth :<|> patchItem ) = client (Proxy @OnePasswordAPI)



newtype OnePasswordException
  = OnePasswordClientFailure ClientError
  deriving (Eq, Generic, Show, Exception)

automationVault :: VaultID
automationVault = "nv5egw46tdohylikpvxaqsnaoa"

newtype Query1P = Query1P {query1P :: forall a. (Token -> ClientM a) -> IO a}

mkQuery1P :: Text -> Int -> Token -> IO Query1P
mkQuery1P host port token = do
  manager <- newManager defaultManagerSettings
  pure $
    Query1P $ \f -> do
      result <- runClientM
        do f token
        do mkClientEnv manager $ BaseUrl Http (toS host) port ""
      either (throwIO . OnePasswordClientFailure) pure result

useOnePassword :: FilePath -> IO Query1P
useOnePassword configPath = do
  config :: Value <- decodeFileThrow $ encodeString configPath
  maybe (panic "no configuration for one password") pure
    <=< sequenceA
    $ mkQuery1P
      <$> config ^? key "hostname" . _String
      <*> config ^? key "port" . _Integral
      <*> config ^? key "token" . _String . L.to Token

-- example
healthGet :: Query1P -> IO Value
healthGet q1p = query1P q1p getHealth

